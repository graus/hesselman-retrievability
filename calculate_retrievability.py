import sys
import elasticsearch
from collections import defaultdict
import urllib
import urllib2
import json

# Nodig:
# - ES interface (standaard: http://elasticsearch-py.readthedocs.org/)
# - Lijst met queries (ga nu uit van 1 query per regel txt file)
# - Lijst met docIds (voor later)
# - Dictionary waarin we de resultaten bewaren, zie;
#   https://docs.python.org/2/library/collections.html#defaultdict-examples
#   de value is standaard een float (counter), elke keer als een query een docId 
#   "retrieved", tellen we er 1 bij op.

c = 25 # cutoff
querylist = open("query_sample.json", "rb") # hier gaan de json logfiles
docIdList = [d.strip() for d in open("docids.txt", "rb").readlines()]
resultDict = defaultdict(float)

num_queries = 0.
for q in querylist:
  num_queries += 1. # tel het antal queries
  q = eval(q)
  query = q["query"]
  results = q["results"]
  for doc_id, n_retrieved in results.items():
    resultDict[doc_id] += n_retrieved # hier voegen we het resultaat toe aan het dict

# Nu hebben we een dictionary met voor elke docId het aantal queries die het 
# document heeft 'geretrieved'. Die normaliseren we (delen door het totaal
# aantal queries). De docIds die nooit zijn geretrieved hebben een 
# retrievability van 0.
# We vervolgens een tab separated values file schrijven die per docId het aantal
# queries en de genormaliseerde retrievability bevat.

print "docId\tn_queries\tretrievability" # HEADER
for d in docIdList:
  d = d.strip()
  if d in resultDict:
    n_retrievability = resultDict[d] / num_queries
    print str(d) +  "\t" +  str(resultDict[d]) + "\t" + str(n_retrievability)
  else:
    print str(d) + "\t0\t0.0"
    continue

